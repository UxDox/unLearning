package minggu3;

public class faktorial {
    public int nilai, hasil;

    public void faktorialBF(int n){
        nilai = n;
        int fakto = 1;
        for (int i = 1; i <= n; i++){
            fakto = fakto * i;
            nilai = fakto;
        }
    }

    public void faktorialDC(int n){
        nilai = n;
        if (n == 1){
            nilai = 1;
        } else {
            int fakto = n * faktorialDC(n-1);
            nilai = fakto;
        }
    }
}
