package minggu3;
import java.util.Scanner;

public class minMaxMain {
    public static void main(String[] args) {
        minMax[] ppArray = new minMax[5];
        ppArray[0] = new minMax();
        minMaxHitung proses = new minMaxHitung();
        Scanner sc = new Scanner(System.in);

        // input
        for (int i = 0; i < 5; i++) {
            ppArray[i] = new minMax();
            System.out.println("Nilai Array indeks  ke-" + i);
            System.out.print("Masukkan nilai: ");
            ppArray[i].nilaiArray = sc.nextInt();
        }

        int min = ppArray[0].nilaiArray;
        int max = ppArray[0].nilaiArray;
        for (int i = 0; i < 5; i++){
            min = proses.hitungMin(ppArray[i].nilaiArray, min);
            max = proses.hitungMax(ppArray[i].nilaiArray, max);
        }

        System.out.println("Brute Force");
        System.out.println("Nilai Minimal: " + min);
        System.out.println("Nilai Maximal: " + max);
        int arr[] = new int[5];
        for (int i = 0; i < 5; i++) arr[i] = ppArray[i].nilaiArray;
        Maxmin hasil = new Maxmin();
        minMax.max_min(arr, 0, 4, hasil);
        System.out.println("Divide Conquer");
        System.out.print("Nilai Minimal: "+hasil.minimum+"\nNilai Maksimal: "+hasil.maximum);
        System.out.print("\n");
    }
}
