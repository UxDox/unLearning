package minggu3;

import java.util.Scanner;

public class faktorialMain {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        faktorial fakObjek = new faktorial();
        System.out.print("Masukkan jumlah n faktorial: ");
        fakObjek.nilai = sc.nextInt();

        System.out.println("Nilai faktorial Brute Force: "+fakObjek.faktorialBF(fakObjek.nilai));
        System.out.println("Nilai faktorial Divide Conquer: "+fakObjek.faktorialDC(fakObjek.nilai));
    }
}
