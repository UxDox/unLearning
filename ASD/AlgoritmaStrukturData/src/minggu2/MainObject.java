package minggu2;
//no1
import java.util.Scanner;

public class MainObject {
    public static void main(String[] args){
        JajarGenjang[] jgArray = new JajarGenjang[5];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 5; i++){
            System.out.println("Jajar Genjang ke-" + i);
            System.out.print("Panjang = ");
            int p = sc.nextInt();
            System.out.print("Tinggi = ");
            int t = sc.nextInt();
            System.out.print("Sisi miring = ");
            int sm = sc.nextInt();
            jgArray[i] = new JajarGenjang(p, t, sm);
        }

        for(int i = 0; i < 5; i++){
            System.out.println("Jajar Genjang ke-" + i);
            System.out.println("Luas: " + jgArray[i].panjang + " * " + jgArray[i].tinggi + " = " + jgArray[i].hitungLuas() +
                    "\nKeliling: (" + jgArray[i].panjang + " *2) + (" + jgArray[i].sisimiring + " *2) = " + jgArray[i].hitungKeliling());
        }
    }
}
