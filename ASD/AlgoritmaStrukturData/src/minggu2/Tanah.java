package minggu2;
// no2
import java.util.Scanner;

public class Tanah {
    int panjang;
    int lebar;

    Tanah(int p, int l){
        panjang = p;
        lebar = l;
    }

    int luasTanah(){
        return panjang*lebar;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Jumlah Tanah: ");
        int jl = sc.nextInt();
        Tanah[] land = new Tanah[jl];

        for (int i = 0; i < jl; i++){
            System.out.println("Tanah "+ i+1);
            System.out.print("Panjang: ");
            int p = sc.nextInt();
            System.out.print("Lebar: ");
            int l = sc.nextInt();
            land[i] = new Tanah(p,l);
        }

        for (int i = 0; i < jl; i++){
            System.out.println("Luas Tanah "+(i+1)+" : "+land[i].luasTanah());
        }
    }
}
