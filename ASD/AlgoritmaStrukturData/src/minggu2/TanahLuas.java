package minggu2;
//no4
import java.util.Scanner;

public class TanahLuas {
    int panjang;
    int lebar;

    TanahLuas(int p, int l){
        panjang = p;
        lebar = l;
    }

    int luasTanah(){
        return panjang*lebar;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Jumlah Tanah: ");
        int jmlTanah = sc.nextInt();
        TanahLuas[] land = new TanahLuas[jmlTanah];

        for (int i = 0; i < jmlTanah; i++) {
            System.out.println("Tanah " + i + 1);
            System.out.print("Panjang: ");
            int p = sc.nextInt();
            System.out.print("Lebar: ");
            int l = sc.nextInt();
            land[i] = new TanahLuas(p, l);
        }

        for (int i = 0; i < jmlTanah; i++) {
            System.out.println("Luas Tanah " + (i + 1) + " : " + land[i].luasTanah());
        }

        int max = 0;
        for (int i = 0; i < jmlTanah; i++){
            if (max < land[i].luasTanah()){
                max = land[i].luasTanah();
            }
        }
        System.out.println("Tanah terluas: "+ max);
    }
}
