package minggu2;
//no1
public class JajarGenjang {
    int tinggi, panjang, sisimiring;
    JajarGenjang(int t, int p, int sm){
        tinggi = t;
        panjang = p;
        sisimiring = sm;
    }
    int hitungLuas(){
        return panjang*tinggi;
    }
    int  hitungKeliling(){
        return (panjang*2)+(sisimiring*2);
    }
}
