package minggu1;

public class Peminjaman {
    String id, namaMember, namaGame;
    int lamaPinjam, hargaSewa;

    Peminjaman(String id, String nama, String game,
               int lama, int harga){
        this.id = id;
        this.namaMember = nama;
        this.namaGame = game;
        this.lamaPinjam = lama;
        this.hargaSewa = harga;
    }

    void tampilPeminjaman(){
        System.out.println("id = "+id);
        System.out.println("Nama Member = "+namaMember);
        System.out.println("Nama Game = "+namaGame);
        System.out.println("Lama Pinjam = "+lamaPinjam);
        System.out.println("Harga Sewa = Rp."+(hargaSewa*lamaPinjam));
    }

    public static void main(String[] args){
        Peminjaman Kasir = new Peminjaman("1337", "Unero", "RE 2", 15, 5000);
        Kasir.tampilPeminjaman();
        /*          Looping
        Scanner input = new Scanner(System.in);
        Peminjaman Kasir = new Peminjaman();
        boolean next = true;
        while (next){
            Kasir.addView();
            String choice = input.next();
            if (choice.equals("Tidak")){
                next = false;
            }
        }
        */
    }
}
