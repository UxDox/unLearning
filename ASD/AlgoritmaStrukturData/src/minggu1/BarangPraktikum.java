package minggu1;

public class BarangPraktikum {
    String nama;
    int hargaSatuan;
    int jumlah;

    BarangPraktikum(){}

    private int hitungHargaTotal(int hargaSatuan, int jumlah){
        return hargaSatuan*jumlah;
    }

    private int hitungDiskon(int total){
        if (total >= 50000 && total <= 100000){
            return (total*5)/100;
        } else if (total > 100000){
            return (total*10)/100;
        } else {
            return 0;
        }
    }

    private int hitungHargaBayar(int total, int diskon){
        return total - diskon;
    }

    public static void main(String[] args){
        BarangPraktikum hit = new BarangPraktikum();
        int total = hit.hitungHargaTotal(10000, 5);
        int diskon = hit.hitungDiskon(total);
        System.out.println(total + " - " + diskon + " = " + hit.hitungHargaBayar(total,diskon));
    }
}
