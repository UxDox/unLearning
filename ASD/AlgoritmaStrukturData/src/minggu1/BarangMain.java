package minggu1;

public class BarangMain {
    public static void main(String[] args){
        Barang b1 = new Barang();
        //Barang b2 = new Barang("Logitech G102", "Mouse", 4, 120000);
        b1.namaBarang = "Corsair 2GB";
        b1.jenisBarang = "DDR";
        b1.hargaSatuan = 250000;
        b1.stok = 10;
        b1.tambahStok(1);
        b1.kurangiStok();
        b1.tampilBarang();
        //b2.tampilBarang();
        int hargaTotal = b1.hitungHargaTotal(4);
        System.out.println("Harga 4 buah = "+hargaTotal);
    }
}
