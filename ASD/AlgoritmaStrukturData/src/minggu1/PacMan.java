package minggu1;

import java.util.Scanner;

public class PacMan {
    private int x, y, width, height;

    private PacMan(int x, int y, int maxX, int maxY){
        this.x = x;
        this.y = y;
        this.width = maxX;
        this.height = maxY;
    }

    private void moveLeft(){x-=1;}
    private void moveRight(){x+=1;}
    private void moveUp(){y+=1;}
    private void moveDown(){y-=1;}
    private void printPosition(){
        if (x < 0 || x > width){
            System.out.println("Keluar dari area!");
            if (x < 0) x+=1;
            else x-=1;
        } else if (y < 0 || y > height){
            System.out.println("Keluar dari area!");
            if (y < 0) y+=1;
            else y-=1;
        }
        System.out.println(x+","+y);
    }

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        PacMan pos = new PacMan(0,0, 15, 15);
        boolean q = true;

        while (q){
            String keyPress = input.next();
            switch (keyPress) {
                case "w":
                    pos.moveUp();
                    pos.printPosition();
                    break;
                case "s":
                    pos.moveDown();
                    pos.printPosition();
                    break;
                case "d":
                    pos.moveRight();
                    pos.printPosition();
                    break;
                case "a":
                    pos.moveLeft();
                    pos.printPosition();
                    break;
                case "q":
                    q = false;
                    break;
            }
        }
        System.out.print("Anda keluar dalam game, posisi terakhir anda ");
        pos.printPosition();
    }
}
