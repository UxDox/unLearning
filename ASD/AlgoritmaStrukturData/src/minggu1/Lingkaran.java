package minggu1;

class Lingkaran {
    double PHI = 3.14; //const
    double r;

    private Lingkaran(){}

    private double hitungLuas(double r){
        return PHI*r*r;
    }

    private double hitungKeliling(double r){
        return PHI*2*r;
    }

    public static void main(String[] args){
        Lingkaran lingkaranku = new Lingkaran();
        double r = 10;
        System.out.println(lingkaranku.hitungLuas(r) +
                "\n" + lingkaranku.hitungKeliling(r));
    }
}
