/**
 * deretDescendingRekursif
 */
public class deretDescendingRekursif {
    public static void descRekursif(int i) {
        if (i >= 0) {
            System.out.print(i + " ");
            descRekursif(i-1);
        }
    }
    public static void main(String[] args) {
        descRekursif(10);
    }    
}