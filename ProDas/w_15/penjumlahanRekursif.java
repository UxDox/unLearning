/**
 * penjumlahanRekursif
 */
public class penjumlahanRekursif {
    static int count = 1;
    static int sum = 0;
    public static int penjumlah(int angka) {
        if (angka >= 1) {
            if (angka == 1) {
                System.out.print(count + " = ");
            } else {
                System.out.print(count + " + ");
            }
            count+=1;
            return angka + penjumlah(angka-1);
        } else{
            return 0;
        }
    }    

    public static void main(String[] args) {
        System.out.print(penjumlah(4));
    }
}