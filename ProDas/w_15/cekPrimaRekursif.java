/**
 * cekPrimaRekursif
 */
import java.util.*;
public class cekPrimaRekursif {
    
    public static boolean primeNum(int angka, int modulus) {
        if (angka % modulus == 0) {
            return false;
        } 
        if (modulus * modulus > angka){
            return true;
        }
        return primeNum(angka, modulus+1);
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int bil = input.nextInt();
        if (primeNum(bil,2)) {
            System.out.print("Ya");
        } else {
            System.out.print("Tidak");
        }
    }   
}