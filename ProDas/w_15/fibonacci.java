/**
 * fibonacci
 */
import java.util.Scanner;
public class fibonacci {
    static int n1 = 0;
    static int n2 = 1;
    static int n3 = 0;
    public static void fibonacciRekursif(int angka) {
        if (angka>0) {
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            System.out.print(" " + n3);
            fibonacciRekursif(angka-1);
        }
    }
    
    public static void fibonacciIteratif(int angka) {
        System.out.print(n1 + " " + n2);
        int n1 = 0;
        int n2 = 1;
        int n3 = 0;
        for (int i = angka; i > 0; i--) {
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            System.out.print(" " + n3);
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int pilihan = input.nextInt();
        if (pilihan == 1) {
            System.out.print(n1 + " " + n2);
            fibonacciRekursif(n-2);
        } else if (pilihan == 2) {
            fibonacciIteratif(n-2);
        } else {
            System.out.print("Hohhohohohoho");
        }
    }
}