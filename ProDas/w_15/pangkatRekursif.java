/**
 * pangkatRekursif
 */
public class pangkatRekursif {
    static int sum = 1;
    public static void multiplier(int angka, int pangkat) {
        if (pangkat >= 1) {
            System.out.print(angka + "x");
            sum*=angka;
            multiplier(angka, pangkat-1);
            if (pangkat == 1) {
                System.out.print(" = " + sum);
            }
        } else {
            System.out.print("1");
        }
    }

    public static void main(String[] args) {
        multiplier(2, 5);
    }
}