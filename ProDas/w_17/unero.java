/**
 * unero
 */
import java.util.*;
public class unero {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan nilai sisi: ");
        float s = input.nextFloat();
        System.out.print("Masukkan nilai tinggi: ");
        float t = input.nextFloat();

        if (s > 0 && t > 0) {
            System.out.print("====== Menu ======\n1. Luas Permukaan\n2. Volume\n");
            System.out.print("Masukkan pilihan: ");
            int pilih = input.nextInt();
            switch (pilih) {
                case 1:
                    System.out.print("Luas Permukaan: " + luasPermukaan(s, t));
                    break;
                case 2:
                    System.out.print("Volume: " + volume(s, t));
                    break;
                default:
                    System.out.print("Pilihan anda salah! 1 atau 2");
                    break;
            }
        }
    }
    // function
    static double luasPermukaan(float s, float t) { 
        double luasPersegi = s * s;
        double luasSegitiga = 0.5 * (s/2) * Math.sqrt(Math.pow(s/2, 2) + Math.pow(t, 2)); // 1/2*a*tSisi
        double luasP = luasPersegi + (4 * luasSegitiga);
        return luasP;
    }
    
    static double volume(float s, float t) {
        double vol = 0.33*s*s*t;
        return vol;
    }
}