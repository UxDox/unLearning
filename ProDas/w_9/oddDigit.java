/**
 * oddDigit
 */
import java.util.Scanner;
public class oddDigit {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int n = input.nextInt();
    int count = 0;
    System.out.printf("N = %d", n);
    while(n > 0){
        int rem = n % 10;
        if (rem % 2 != 0) {
            count++;
        }
        n /= 10;
    }
    System.out.printf(", jumlah digit yang ganjil = %d", count );
  }
}