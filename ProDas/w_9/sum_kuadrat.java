/**
 * sum_kuadrat
 */
import java.util.*;
public class sum_kuadrat {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("N= ");
        int N = input.nextInt();
        int j = 1, k =1, jum = 0;
        for (int i = 0; i <N; i++) {
            if (i==N-1) {
                System.out.printf("%.0f", Math.pow(j, 2));
            } else {
                System.out.printf("%.0f +", Math.pow(j, 2));
            }
            jum+=(j*k);
            j++;
            k++;
        }
        System.out.print("= "+ jum);
    }
}