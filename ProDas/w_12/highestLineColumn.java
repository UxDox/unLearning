/**
 * highestLineColumn
 */
import java.util.*;
public class highestLineColumn {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int highestBaris = 0;
        int arr[][] = new int[3][5];

        for (int Outer = 0; Outer < 3; Outer++) {
            for (int Inner = 0; Inner < 5; Inner++) {
                System.out.printf("Input Index - [%d][%d]: ", Outer, Inner);
                arr[Outer][Inner] = input.nextInt();
                if (arr[Outer][Inner] > highestBaris) {
                    highestBaris = arr[Outer][Inner];
                }
            }
            System.out.printf("Bilangan terbesar pada baris ke - %d: %d\n", Outer, highestBaris);
        }

        for (int Inner = 0; Inner < 5; Inner++) {
            int highestKolom = 0;
            for (int Outer = 0; Outer < 3; Outer++) {
                if (arr[Outer][Inner] > highestKolom) {
                    highestKolom = arr[Outer][Inner];
                }
            }
            System.out.printf("%d, adalah bilangan terbesar pada kolom ke - %d\n",highestKolom, Inner);
        }
    }
}