/**
 * sumArr
 */
import java.util.*;
public class sumArr {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int arr[][] = new int[4][5];
        int total = 0;

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.printf("Input index-[%d][%d]: ", i,j);
                arr[i][j] = input.nextInt();
            }
        }
        System.out.print("--------------------------------------\n");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                total += arr[i][j];
            }
        }
        System.out.print("Total: "+ total);
    }
}