/**
 * MatrixTranspose
 */
import java.util.*;
public class MatrixTranspose {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int Outer = input.nextInt();
        int Inner = input.nextInt();
        int arr[][] = new int[Outer][Inner];
        int arrTranspose[][] = new int[Inner][Outer];

        for (int baris = 0; baris < arr[0].length; baris++) {
            for (int kolom = 0; kolom < arr[1].length; kolom++) {
                System.out.printf("Index - [%d][%d]: ", baris, kolom);
                arr[baris][kolom] = input.nextInt();
                arrTranspose[kolom][baris] = arr[baris][kolom];
            }
        }
        
        for (int barisT = 0; barisT < arrTranspose[0].length; barisT++) {
            for (int kolomT = 0; kolomT < arrTranspose[1].length; kolomT++) {
                System.out.print(arrTranspose[barisT][kolomT]);;
            }
            System.out.println();
        }
    }
}