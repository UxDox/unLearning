/**
 * nilai_max
 */
import java.util.*;
public class nilai_max {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int arr[][] = new int[3][4];
        int max = 0;
        for (int baris = 0; baris < arr[0].length-1; baris++) {
            for (int kolom = 0; kolom < arr[1].length; kolom++) {
                System.out.printf("Masukkan nilai index ke - [%d][%d]: ", baris, kolom);
                arr[baris][kolom] = input.nextInt();
            }
        }

        for (int i = 0; i < arr[0].length-1; i++) {
            for (int j = 0; j < arr[1].length; j++) {
                if (arr[i][j] > max) {
                    max = arr[i][j];
                }
            }
        }
        System.out.printf("Bilangan terbesar adalah %d, pada index %d",max, Arrays.asList(arr).indexOf(max));
    }
}