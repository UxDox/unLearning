/**
 * NumberSquare
 */
import java.util.*;
public class NumberSquare {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int angka = input.nextInt();
        for (int baris = 1; baris <= angka; baris++) {
            for (int kolom = 1; kolom <= angka; kolom++) {
                if ((baris>1&&baris<angka) && (kolom>1 && kolom<angka)) {
                    System.out.print(" ");
                } else{
                    System.out.print(angka);
                }
            }
            System.out.println();
        }
    }
}