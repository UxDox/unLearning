/**
 * OppositeNumber
 */
import java.util.*;
public class OppositeNumber {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        if (N>=2) {
            for (int baris = 1; baris <= N; baris++) {
                if (baris%2!=0) {
                    for (int odd = 1; odd <=N; odd++) {
                        System.out.print(odd);
                    }
                    System.out.println();
                } else {
                    for (int even = N; even >=1; even--) {
                        System.out.print(even);
                    }
                    System.out.println();
                }
            }
        } else{
            System.out.print("Inputan minimal 2");
        }
    }
}