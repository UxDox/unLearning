/**
 * PrimeNumber
 */
import java.util.*;
public class PrimeNumber {
    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);
        String prima = "";
        int angka = input.nextInt();
        for (int deret = 1; deret <= angka*2; deret++) {
            int count = 0;
            for (int bilangan = deret; bilangan >= 1; bilangan--) {
                if (deret%bilangan==0) {
                    count += 1;
                }
            }
            if (count == 2) {
                prima = prima + deret + " ";
            }
        }
        System.out.print(prima);
    }
} 