/**
 * tugas1
 */
import java.util.*;
public class NumberTriangle {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan jumlah: ");
        int N = input.nextInt();
        for (int baris = 1; baris <= N; baris++) {
            for (int kolomIsiSpasi = N; kolomIsiSpasi > baris; kolomIsiSpasi--) {
                System.out.print(" ");
            }
            for (int kolomIsiAngka = 1; kolomIsiAngka <= baris; kolomIsiAngka++) {
                System.out.print(kolomIsiAngka);
            }
            System.out.println();
        }
    }
}