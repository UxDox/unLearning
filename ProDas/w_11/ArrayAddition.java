/**
 * ArrayAddition
 */
import java.util.*;
public class ArrayAddition {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int arr1[] = new int[3];
	    int arr2[] = new int[3];

	    for(int i = 1; i <= arr1.length; i++){
            System.out.printf("Masukkan arr1[%d]: ", i);
            arr1[i-1] = input.nextInt();
            System.out.printf("Masukkan arr2[%d]: ", i);
            arr2[i-1] = input.nextInt();
        }
 
        int arrSum[] = new int[3];
        for (int i = 1; i <= arrSum.length; i++) {
            arrSum[i-1] = arr1[i-1] + arr2[i-1];
            System.out.printf("Penjumlahan ke-%d: %d + %d = %d\n", i, arr1[i-1], arr2[i-1], arrSum[i-1]);
        }    
    }
}