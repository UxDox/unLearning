/**
 * ReversedArray
 */
import java.util.*;
public class ReversedArray {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] elemenArray = new int[4];
        for (int i = 1; i <= 4; i++) {
            System.out.printf("Masukkan Nilai index ke-%d: ", i);
            elemenArray[i-1] = input.nextInt();
        }
        
        for (int i = 4; i >= 1; i--) {
            System.out.printf("Nilai index ke-%d: %d\n", i, elemenArray[i-1]);
        }
    }
}