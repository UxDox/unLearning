/**
 * ArraySum
 */
import java.util.*;
public class ArraySum {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan jumlah isi array: ");
        int jumlah = input.nextInt();
        int arr[] = new int[jumlah];
        int total = 0;
        for (int i = 1; i <= jumlah; i++) {
            System.out.printf("Masukkan array ke-%d: ",i);
            arr[i-1] = input.nextInt();
            total += arr[i-1];
        }
        System.out.printf("Hasil penjumlahan isi elemen array: ", total);
    }
}