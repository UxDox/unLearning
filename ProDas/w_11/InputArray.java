/**
 * InputArray
 */
import java.util.*;
public class InputArray {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] elemenArray = new int[7];
        for (int i = 1; i <= 7; i++) {
            System.out.printf("Masukkan Nilai index ke-%d: ", i);
            elemenArray[i-1] = input.nextInt();
        }
        
        for (int i = 1; i <= 7; i++) {
            System.out.printf("Nilai index ke-%d: %d\n", i, elemenArray[i-1]);
        }
    }
}