/**
 * ArrayMax
 */
import java.util.*;
public class ArrayMax {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan isi array: ");
        int jumlah = input.nextInt();
        int arr[] = new int[jumlah];
        int max = 0;
        for (int i = 1; i <= jumlah; i++) {
            System.out.printf("Masukkan array ke-%d: ", i-1);
            arr[i-1] = input.nextInt();
            if (arr[i-1] > max) {
                max = arr[i-1];
            }
        }
        System.out.print("Highest: " + max);
    }   
}