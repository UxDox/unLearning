/**
 * ArrayCopy
 */
import java.util.Scanner;
public class ArrayCopy {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] arr1 = new int[4];
        int[] arr2 = new int[4];

        for (int i = 1; i <= 4; i++) {
            System.out.printf("Masukkan nilai index ke-%d: ", i);
            arr1[i-1] = input.nextInt();
            arr2[i-1] = arr1[i-1];
        }

        for (int i = 1; i <= 4; i++) {
            System.out.printf("arr2[%d]: %d\n", i, arr2[i-1]);
        }
    }
}