/**
 * OddEven
 */
import java.util.*;
public class OddEven {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int jumlahArray = input.nextInt();
        int arr[] = new int[jumlahArray];
        int ganjil[] = new int[jumlahArray];
        int genap[] = new int[jumlahArray];
        for (int i = 1; i <= jumlahArray; i++) {
            System.out.printf("Masukkan array ke-%d: ",i-1);
            arr[i-1] = input.nextInt(); 
        }
        System.out.print("\nGanjil\n");
        for (int i = 1; i <= jumlahArray; i++) {
            if (arr[i-1] != 0) {
                System.out.print(arr[i-1]+ "\n");
            }
        }
        System.out.print("\nGenap\n");
        for (int i = 1; i <= jumlahArray; i++) {
            if (arr[i-1] == 0) {
                System.out.print(arr[i-1]+ "\n");
            }
        }
    }
}
