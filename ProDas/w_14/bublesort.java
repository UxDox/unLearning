/**
 * bublesort
 */
public class bublesort {
    public static void swap (int arr[], int j, int temp) {
        if (arr[j-1] > arr[j]) {
            temp = arr[j-1];
            arr[j-1] = arr[j];
            arr[j] = temp;
        }
    }

    public static void sorting(int arr[]) {
        int temp = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 1; j < (arr.length-i); j++) {
                swap(arr, j, temp);
            }
        }
    }

    public static void tampil(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
    public static void main(String[] args) {
        int arr[] = {16, 4, 10, 90, 27, 3, 12, 28};
        sorting(arr);
        System.out.println("Hasil pengurutan: ");
        tampil(arr);
    }
}