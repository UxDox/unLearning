/**
 * prak2
 */
public class prak2 {

    static void beriSalam(){
        System.out.println("Halo! Selamat Pagi");
    }

    static void beriUcapan(String ucapan){
        System.out.println(ucapan);
    }

    public static void main(String[] args) {
        beriSalam();
        String salam = "Привет!";
        beriUcapan(salam);
    }

}