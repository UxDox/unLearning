/**
 * maximus
 */
import java.util.Scanner;
public class maximus {
    
    static void highestNum(int n1, int n2, int n3){
        if (n1 > n2 && n1 > n3) {
            System.out.print("Highest num = " + n1);
        } else if(n2 > n1 && n2 > n3){
            System.out.print("Highest num = " + n2);
        } else if(n3 > n1 && n3 > n1){
            System.out.print("Highest num = " + n3);
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n1 = input.nextInt();
        int n2 = input.nextInt();
        int n3 = input.nextInt();
        highestNum(n1, n2, n3);
    }
}