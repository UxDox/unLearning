/**
 * lingkaran
 */
import java.util.Scanner;
public class lingkaran {

    public static double kelilingLingkaran(float r) {
        double keliling = 3.14 * r * 2.00;
        return keliling;
    }

    public static double luasLingkaran(float r) {
        double luas = 3.14 * r * r;
        return luas;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float r = input.nextFloat();
        System.out.printf("Luas = %.2f - Kelililng = %.2f", luasLingkaran(r), kelilingLingkaran(r));
    }
}